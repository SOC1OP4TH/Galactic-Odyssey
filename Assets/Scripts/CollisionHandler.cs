using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] ParticleSystem crashParticles;

    // Start is called before the first frame update

    void OnTriggerEnter(Collider other){
        StartCrashSequence();
    }
     void StartCrashSequence(){
    crashParticles.Play();
    GetComponent<MeshRenderer>().enabled = false;
    GetComponent<PlayerControls>().enabled = false;
   
    Invoke("ReloadLevel", 1f);
   
       
    }
    void ReloadLevel(){
      int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
}
