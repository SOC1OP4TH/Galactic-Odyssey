using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject death;
    int scorePerHit = 15;


    GameObject parentGameObject;
    ScoreBoard scoreBoard;
    void Start(){
        scoreBoard = FindObjectOfType<ScoreBoard>();
        AddRigidbody();
        parentGameObject = GameObject.FindWithTag("Clones");

    }
    void AddRigidbody(){
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
    }
void OnParticleCollision(GameObject other)
{
    kill();
    scorer();
}
    void kill(){
          //destroy
    GameObject vfx =  Instantiate(death , transform.position , Quaternion.identity);
    Destroy(gameObject);
    vfx.transform.parent = parentGameObject.transform;
    //print("I was hit");
    }
    void scorer(){
      scoreBoard.IncreaseScore(scorePerHit);
    }
 

    
}

