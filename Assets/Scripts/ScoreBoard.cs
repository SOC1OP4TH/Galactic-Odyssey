using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScoreBoard : MonoBehaviour
{
    int score;
    TMP_Text scoreText;
    void Start(){
        scoreText = GetComponent<TMP_Text>();
        scoreText.text = "KEMO ADAM VUR";
    }
    // Start is called before the first frame update
   public void IncreaseScore(int amount){
       score += amount;
       scoreText.text = score.ToString();
    }
}
