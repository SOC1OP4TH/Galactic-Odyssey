using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    // Start is called before the first frame update
   [SerializeField] float controlSpeed = 10f;
    [SerializeField] GameObject[] lasers;
   float positionYawFactor = 2f;
   float controlPitchFactor = -10f;
   float controlRollFactor = -20f;
   float xThrow;
   float yThrow;
   float positionPitchFactor = 2f; 
   

    void Start()
    {
        
    }

    void Update(){
        ProcessTranslation();
        ProcessRotation();
        ProcessFiring();

    }
   
    void ProcessTranslation()
    {
        float xRange = 15f;
        float yRange = 5f;
        xThrow = Input.GetAxis("Horizontal");
        yThrow = Input.GetAxis("Vertical");

        float xOffset = xThrow * Time.deltaTime * controlSpeed;
        float yOffset = yThrow * Time.deltaTime * controlSpeed;

        float newXPos = transform.localPosition.x + xOffset;
        float newYPos = transform.localPosition.y + yOffset;

        newXPos = Mathf.Clamp(newXPos, -xRange, xRange);
        newYPos = Mathf.Clamp(newYPos, -yRange, yRange);


        transform.localPosition = new Vector3( newXPos ,newYPos,transform.localPosition.y);
    }
    void ProcessRotation(){ 
        float pitch = transform.localPosition.y * positionPitchFactor + yThrow*controlPitchFactor;
        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;
        transform.localRotation = Quaternion.Euler(pitch,yaw,roll);
    }
    void ProcessFiring(){
        if(Input.GetButton("Fire1")){
            SetLaserActive(true);
        }
        else{
            SetLaserActive(false);
        }
    }
    void SetLaserActive(bool isActive){
        foreach(GameObject laser in lasers){
            var emissionModule = laser.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
    }



    
}
