This is a simple 3D space shooter game made with Unity. The player controls a spaceship that can move around the world and fire lasers. The goal of the game is to destroy enemies and collect points.

## Controls

* W, A, S, D to move
* Space to fire lasers
## Gameplay

The player must destroy enemies and avoid obstacles in order to collect points. If the player hits an obstacle, they will die and the game will end.

## Enemies

Enemies will spawn randomly around the world. Enemies will fire lasers at the player.

## Points

The player gets points for destroying enemies. The player's score is displayed in the top left corner of the screen.

To win the game, the player must collect a certain number of points (configurable in the game settings).

## Tips

* Use the movement keys to avoid obstacles and enemies.
* Use the spacebar to fire lasers at enemies.
* Be careful not to run out of ammo.
# Enjoy!

### Additional information

The game also includes a collision handler script that will destroy the player's spaceship and reload the level if the player hits an obstacle.

The game also includes a score board script that keeps track of the player's score and displays it in the top left corner of the screen.

The game also includes a particle system that is used to create a visual effect when the player's spaceship is destroyed.
